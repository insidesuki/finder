<?php
declare(strict_types = 1);

namespace Insidesuki\Finder;
use Insidesuki\Finder\Contracts\FinderRepositoryInterface;
use Insidesuki\Finder\Contracts\FinderResultPresentationInterface;

abstract class AbstractFinder
{

	private mixed $presentation;

	public function __construct(protected readonly FinderRepositoryInterface $finderRepository)
	{

		$presentation = static::presentationClass();
		$this->presentation = new $presentation();
	}

	abstract protected static function presentationClass(): string;

	public function find(string $keyword): FinderResultPresentationInterface
	{
		$this->presentation->write($this->finderRepository->searchAndFind($keyword));
		return $this->presentation;

	}

}