<?php

namespace Insidesuki\Finder\Contracts;
interface FinderRepositoryInterface
{

	public function searchAndFind(string $keyword): array;

}