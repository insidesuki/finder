<?php

namespace Insidesuki\Finder\Contracts;
interface FinderResultPresentationInterface
{

	public function write(array $result): void;

	public function read(): array;

}