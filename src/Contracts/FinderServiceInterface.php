<?php

namespace Insidesuki\Finder\Contracts;

interface FinderServiceInterface
{
	public function find(string $keyword): FinderResultPresentationInterface;

	public function finderName():string;
}