<?php
declare(strict_types = 1);

namespace Insidesuki\Finder\Exception;
use InvalidArgumentException;

class EmptyFindersException extends InvalidArgumentException
{

	public function __construct()
	{
		parent::__construct('No finders registered');
	}
}