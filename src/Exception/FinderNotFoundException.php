<?php
declare(strict_types = 1);

namespace Insidesuki\Finder\Exception;
use RuntimeException;

class FinderNotFoundException extends RuntimeException
{

	public function __construct(string $name)
	{
		parent::__construct(sprintf('Finder :%s, was not registered', $name));
	}

}