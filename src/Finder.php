<?php
declare(strict_types = 1);

namespace Insidesuki\Finder;
use Insidesuki\Finder\Contracts\FinderServiceInterface;
use Insidesuki\Finder\Exception\EmptyFindersException;
use Insidesuki\Finder\Exception\FinderNotFoundException;
use Insidesuki\Finder\Result\FinderResult;

class Finder
{
	protected string $keysearch;
	private array    $finders = [];

	private array $result = [];

	public function __construct(array $finders = [])
	{

		foreach ($finders as $finder) {
			$this->registerFinder($finder);
		}

	}


	public function handle(string $keysearch, string $context = 'all'): array
	{

		$this->keysearch = $keysearch;
		if(empty($this->finders)) {
			throw new EmptyFindersException();
		}
		if('all' !== $context) {
			$this->runFinder($context);
			return $this->result;
		}
		foreach ($this->finders as $name => $class) {
			$this->runFinder($name);
		}
		return $this->result;

	}

	private function runFinder(string $name): void
	{

		$finderService = @$this->finders[$name];
		if(!isset($finderService)) {
			throw new FinderNotFoundException($name);
		}
		$this->result[] = new FinderResult($name, $finderService->find($this->keysearch));


	}

	private function registerFinder(FinderServiceInterface $finder)
	{

		$this->finders[$finder->finderName()] = $finder;
	}

}