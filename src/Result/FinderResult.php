<?php
declare(strict_types = 1);

namespace Insidesuki\Finder\Result;
use Insidesuki\Finder\Contracts\FinderResultPresentationInterface;

class FinderResult
{

	public readonly int    $total;
	public readonly array  $result;
	public readonly string $name;

	public function __construct(string $name ,FinderResultPresentationInterface $resultPresentation){

		$this->name   = $name;
		$this->result = $resultPresentation->read();
		$this->total  = count($this->result);

	}

}